<?php
require __DIR__ . '/vendor/autoload.php';

$cli = new \App\Build($argv);

if (empty($cli->getArgument(0))) {
    $cli->execute();
    exit;
}

try {
    $serviceFactory = (new \App\ServiceFactory())->make($cli->getOption('service') ?? '');
    $service = new \App\Service($serviceFactory, $cli->getArgument(0), $cli->getArgument(1));

    $cli->out($service->execute());
} catch (\App\ServiceException $e) {
    $cli->err($e->getMessage());
} catch (Exception $e) {
    $cli->err('System error: ' . $e->getMessage());
}
