<?php namespace App;

use App\Services\ServiceInterface;

class Service
{
    private const COMMAND = "git ls-remote %REPOSITORY_LINK% | grep refs/heads/%BRANCH%| cut -f 1";
    private const DEFAULT_BRANCH = 'master';

    private $service;
    private $repository;
    private $branch;

    public function __construct(ServiceInterface $service, string $repository, ?string $branch)
    {
        $this->setService($service);
        $this->setRepository($repository);
        $this->setBranch($branch);
    }

    public function getService(): ServiceInterface
    {
        return $this->service;
    }

    public function setService(ServiceInterface $service): void
    {
        $this->service = $service;
    }

    public function getRepository(): string
    {
        return $this->repository;
    }

    public function setRepository(string $repository): void
    {
        if ('' === $repository){
            throw new \Exception('Repository cannot be empty');
        }
        $this->repository = $repository;
    }

    public function getBranch(): string
    {
        return $this->branch;
    }

    public function setBranch(?string $branch): void
    {
        $this->branch = $branch ?? static::DEFAULT_BRANCH;
    }

    public function execute()
    {
        return exec($this->prepareCommand());
    }

    protected function prepareCommand()
    {
        return str_replace(
            '%REPOSITORY_LINK%',
            $this->service->getFullServiceLink($this->repository),
            str_replace(
                '%BRANCH%',
                $this->branch,
                static::COMMAND
            )
        );
    }
}
