<?php namespace App;

use Asika\SimpleConsole\Console;

class Build extends Console
{
    protected $help = <<<HELP
[Usage] php app.php --service=service_name owner/repo branch

[Options]
    h | help     Show help information
    s | service  Define service for repository
HELP;

    protected function doExecute ()
    {
        $this->out($this->getHelp());

        return true;
    }
}
