<?php namespace App;

use App\Services\Github;
use App\Services\ServiceInterface;

class ServiceFactory
{
    protected const GITHUB = 'github.com';

    public function make(string $serviceName = ''): ServiceInterface
    {
        if (static::GITHUB === $serviceName || '' === $serviceName) {
            return new Github();
        }

        throw new ServiceException("Unknown service: \e[0;31m" . $serviceName . "\e[0m");
    }
}
