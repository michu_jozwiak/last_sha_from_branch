<?php namespace App\Services;

interface ServiceInterface
{
    public function getFullServiceLink(string $repository): string;
}