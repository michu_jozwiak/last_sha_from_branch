<?php namespace App\Services;

class Github implements ServiceInterface
{
    protected const SERVICE_LINK_PATTERN = "git://github.com/%PATTERN%.git";

    public function getFullServiceLink(string $repository): string
    {
        return str_replace('%PATTERN%', $repository, static::SERVICE_LINK_PATTERN);
    }
}
